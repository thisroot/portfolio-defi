export const infoTooltips = {
  data() {
    return {
      claimModalTitle: {
        content: "Unclaimed rewards balance in eRSDL",
        offset: "5px",
        placement: "right"
      },
      borrowMarketInfo: {
        content:
          "Balance - Total value of borrowed assets % of limit - Percentage of borrow used against supplied assets",
        offset: "5px",
        placement: "right"
      },
      borrowBalanceInfo: {
        content: "Total balance of borrowed assets (in USD)",
        offset: "5px",
        placement: "right"
      },
      totalSupplyBalanceInfo: {
        content: "Total balance of supplied assets (in USD)",
        offset: "5px",
        placement: "right"
      },
      netApyInfo: {
        content: "Net interest earned and paid on your assets",
        offset: "5px",
        placement: "right"
      },
      supplyMarketInfo: {
        content: "Balance - Total value of supplied assets",
        offset: "5px",
        placement: "right"
      },
      supplyAPYInfo: {
        content: "Estimated interest earned on supplied Asset",
        offset: "5px",
        placement: "right"
      },
      supplyAPYEarnedInfo: {
        content: "Estimated interest earned on supplied Asset",
        offset: "5px",
        placement: "right"
      },
      supplyBalanceInfo: {
        content: "Balance available for Supply",
        offset: "5px",
        placement: "right"
      },
      supplyWalletInfo: {
        content: "Balance available for Supply",
        offset: "5px",
        placement: "right"
      },
      supplyCollateralInfo: {
        content: "Use an Asset towards your borrowing limit",
        offset: "5px",
        placement: "right"
      },
      borrowLimitInfo: {
        content: "Borrow limit info",
        offset: "5px",
        placement: "right"
      },
      borrowAPYInfo: {
        content: "Estimated interest paid on borrowed Asset",
        offset: "5px",
        placement: "right"
      },
      borrowWalletInfo: {
        content: "Balance available for Borrow",
        offset: "5px",
        placement: "right"
      },
      borrowLiquidityInfo: {
        content: "Balance equivalent available for Borrow(in USD)",
        offset: "5px",
        placement: "right"
      },
      supplyModalSupplyApy: {
        content: "Estimated interest earned on supplied Asset",
        offset: "5px",
        placement: "right"
      },
      supplyModalDistributionApy: {
        content: "Earning rate for eRSDL rewards",
        offset: "5px",
        placement: "right"
      },
      supplyModalBorrowLimit: {
        content: "Total value of borrowed assets",
        offset: "5px",
        placement: "right"
      },
      supplyModalBorrowLimitUsed: {
        content: "Percentage of borrow used against supplied assets",
        offset: "5px",
        placement: "right"
      },
      borrowModalSupplyApy: {
        content: "Estimated interest paid on borrowed Asset",
        offset: "5px",
        placement: "right"
      },
      borrowModalDistributionApy: {
        content: "Earning rate for eRSDL rewards",
        offset: "5px",
        placement: "right"
      },
      borrowModalBorrowBalance: {
        content: "Total value of borrowed assets",
        offset: "5px",
        placement: "right"
      },
      borrowModalBorrowLimitUsed: {
        content: "Percentage of borrow used against supplied assets",
        offset: "5px",
        placement: "right"
      }
    };
  }
};
