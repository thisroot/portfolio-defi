import { mapActions, mapGetters } from "vuex";
import Big from "big.js";
import BaseToggleButton from "@/components/Base/BaseToggleButton";

export default {
  components: {
    BaseToggleButton
  },
  data() {
    return {
      cTokenContract: null
    };
  },
  watch: {
    item: {
      deep: true,
      handler(newValue) {
        this.isEnteredTheMarket = newValue.isEnteredTheMarket;
      }
    }
  },
  computed: {
    ...mapGetters("global", ["ethAccount", "totalBorrow", "borrowLimit"]),
    apy() {
      return this.isSupply ? this.item.supplyApy : this.item.borrowApy;
    },
    borrowLimitPercentage() {
      const limit = (this.totalBorrow / this.borrowLimit) * 100;

      return Number.isNaN(limit) ? 0 : parseFloat(limit.toFixed(2));
    },
    hypotheticalBorrowLimitPercentage() {
      const limit = (this.totalBorrow / this.hypotheticalBorrowLimit) * 100;

      return Number.isNaN(limit) ? 0 : parseFloat(limit.toFixed(2));
    },
    hypotheticalBorrowLimit() {
      const supply = Big(this.item.fiatSupply).times(this.collateralMantissa);
      const borrowLimit = Big(this.borrowLimit);

      return this.isEnteredTheMarket
        ? Number(borrowLimit.minus(supply))
        : Number(borrowLimit.plus(supply));
    },
    isCollateralRequired() {
      return this.isSupply && this.hypotheticalBorrowLimit < this.totalBorrow;
    },
    collateralMantissa() {
      return this.item.collateralFactorMantissa / 1e18;
    }
  },
  methods: {
    ...mapActions("modal", [
      "setCollateralModal",
      "setSupplyModal",
      "closeModal",
      "setBorrowModal"
    ]),
    openModal() {
      const sharedParams = {
        ethAccount: this.ethAccount,
        symbol: this.item.symbol,
        name: this.item.name,
        apy: this.item.apy,
        cToken: this.item.cToken,
        token: this.item.token,
        balance: this.item.balance,
        enteredMarket: this.isEnteredTheMarket,
        tokenUsdValue: this.item.tokenUsdValue,
        exchangeRateCurrent: this.item.exchangeRateCurrent,
        isAllowed: Big(this.item.tokenAllowance).gt(0)
      };

      if (this.isSupply) {
        this.setSupplyModal({
          ...sharedParams,
          supplyApy: this.item.supplyApy,
          supplied: Big(this.marketBalance),
          distributionApy: this.item.supplyDistributionApy,
          collateralMantissa: this.collateralMantissa
        });
      } else {
        this.setBorrowModal({
          ...sharedParams,
          borrowing: Big(this.marketBalance),
          borrowApy: this.item.borrowApy,
          distributionApy: this.item.borrowDistributionApy,
          liquidity: this.item.liquidity
        });
      }
    },
    onChange() {
      const isApproved = Number(this.item.tokenAllowance) > 0;
      const sharedParams = {
        token: this.item.token,
        cToken: this.item.cToken,
        name: this.item.name,
        symbol: this.item.symbol,
        borrowLimitPercentage: this.borrowLimitPercentage,
        marketBalance: Big(this.marketBalance),
        hypotheticalBorrowLimit: this.hypotheticalBorrowLimit
      };

      if (this.isEnteredTheMarket) {
        this.setCollateralModal({
          hypotheticalBorrowLimitPercentage: this
            .hypotheticalBorrowLimitPercentage,
          isRequired: this.isCollateralRequired,
          isAbleToExit: this.item.borrowBalance == 0,
          hypotheticalBorrowLimit: this.hypotheticalBorrowLimit,
          ...sharedParams
        });
      } else {
        this.setCollateralModal({
          isApproved,
          isEnable: true,
          hypotheticalBorrowLimitPercentage: this
            .hypotheticalBorrowLimitPercentage,
          ...sharedParams
        });
      }
    }
  }
};
