import Vue from "vue";
import { upperFirst, toCamelCase } from "@/global/utils";

export const registerInlineIcons = () => {
  const requireComponent = require.context("../assets/inline", true);

  requireComponent.keys().forEach(fileName => {
    const componentConfig = requireComponent(fileName);
    const componentName = upperFirst(
      toCamelCase(
        fileName
          .split("/")
          .pop()
          .replace(/\.\w+$/, "")
      )
    );
    Vue.component(
      componentName + "Icon",
      componentConfig.default || componentConfig
    );
  });
};
