import CONFIG from "@/config/env";

function handleError(response) {
  if (!response.ok) throw new Error(response);

  return response.json();
}

export function getGasFee() {
  return fetch(CONFIG.ETHERSCAN_URL)
    .then(handleError)
    .then(json => json.result.ProposeGasPrice) // response in GWei
    .catch(err => console.error(err));
}

export function getTotalEarned(account) {
  const url = `${CONFIG.API_URL}/profits?addr=${account}`;

  return fetch(url)
    .then(handleError)
    .catch(err => console.error(err));
}

export function getMarketDetails() {
  const url = `${CONFIG.API_URL}/all_markets`;

  return fetch(url)
    .then(handleError)
    .catch(err => console.error(err));
}
