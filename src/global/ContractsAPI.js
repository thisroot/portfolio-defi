import CEther from "@/contracts/CEther";
import CErc20 from "@/contracts/CErc20";
import Faucet from "@/contracts/FaucetToken";
import FaucetNonStandardToken from "@/contracts/FaucetNonStandardToken";
import ComptrollerContract from "@/contracts/Comptroller";
import CompoundLensContract from "@/contracts/CompoundLens";
import { MAX_UINT_256 } from "@/config/constants";
import CONFIG from "@/config/env";
import { providers } from "ethers";

const ethTokenSymbol = "unETH";

export const isEther = async token => {
  const symbol = await CEther(token)
    .methods.symbol()
    .call();

  return symbol === ethTokenSymbol;
};


export class Ctoken {
  constructor(cToken, account, isEth) {
    this.cToken = cToken;
    this.account = account;
    this.isEther = isEth;
    this.methods = isEth ? CEther(cToken).methods : CErc20(cToken).methods;
  }

  supply(value, gas) {
    const params = { from: this.account };
    if (this.isEther) {
      params.value = value;
      params.gas = gas;

      return this.methods.mint().send(params);
    }

    return this.methods.mint(value).send(params);
  }

  getEstimateGas(method, value) {
    return this.methods[method]().estimateGas({ from: this.account, value });
  }

  borrow(value) {
    return this.methods.borrow(value).send({ from: this.account });
  }

  repayBorrow(value) {
    if (this.isEther) {
      return this.methods.repayBorrow().send({
        from: this.account,
        value
      });
    }
    return this.methods.repayBorrow(value).send({ from: this.account });
  }

  withdraw(value) {
    return this.methods.redeemUnderlying(value).send({ from: this.account });
  }

  supplyRatePerBlock() {
    return this.methods.supplyRatePerBlock().call();
  }

  borrowRatePerBlock() {
    return this.methods.borrowRatePerBlock().call();
  }

  getDecimals() {
    return this.methods.decimals().call();
  }

  balanceOfUnderlying() {
    return this.methods.balanceOfUnderlying(this.account).call();
  }

  borrowBalanceCurrent() {
    return this.methods.borrowBalanceCurrent(this.account).call();
  }

  getExchangeRate() {
    return this.methods.exchangeRateStored().call();
  }

  getCash() {
    return this.methods.getCash().call();
  }
}

export class Comptroller {
  constructor(account) {
    this.account = account;
    this.methods = ComptrollerContract(CONFIG.COMPTROLLER_ADDRESS).methods;
    window.web3.eth.net.getNetworkType().then(n => {
      const networkName = n.includes("main") ? "mainnet" : n;
      this.ethers = new providers.Web3Provider(
        window.web3.currentProvider,
        networkName
      );
    });
  }

  waitForTransaction(transactionHash) {
    return this.ethers.waitForTransaction(transactionHash);
  }

  getAllMarkets() {
    return this.methods.getAllMarkets().call();
  }

  enterMarkets(params) {
    return this.methods.enterMarkets(params).send({ from: this.account });
  }

  exitMarket(cToken) {
    return this.methods.exitMarket(cToken).send({ from: this.account });
  }

  checkMembership(cToken) {
    return this.methods
      .checkMembership(this.account, cToken)
      .call({ from: this.account });
  }

  getAccountLiquidity() {
    return this.methods.getAccountLiquidity(this.account).call();
  }

  getHypotheticalAccountLiquidity(cToken, redeemTokens, borrowAmount) {
    return this.methods
      .getHypotheticalAccountLiquidity(
        this.account,
        cToken,
        redeemTokens,
        borrowAmount
      )
      .call();
  }

  claimRewards(tokens) {
    return this.methods
      .claimRewards(this.account, tokens)
      .send({ from: this.account });
  }

  getCompSpeeds(cToken) {
    return this.methods.compSpeeds(cToken).call();
  }
}
