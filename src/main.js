import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Notifications from "vue-notification";
import VTooltip from "v-tooltip";
import ECharts from "vue-echarts";

import BaseInfoBtn from "@/components/Base/BaseInfoBtn";
import { registerInlineIcons } from "@/global/inlineIcons";

Vue.use(Notifications);
Vue.use(VTooltip);
Vue.component("BaseInfoBtn", BaseInfoBtn);
Vue.component("v-chart", ECharts);

registerInlineIcons();

Vue.config.productionTip = false;

async function start() {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount("#app");
}

start().catch(err => {
  alert(err);
});
