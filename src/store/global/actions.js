import {
  SET_LOADER,
  SET_STATE,
  SET_ETH_ACCOUNT,
  SET_TOTAL_BORROW
} from "./mutation-types";

export default {
  setLoader({ commit }, data) {
    commit(SET_LOADER, data);
  },
  setState({ commit }, data) {
    commit(SET_STATE, data);
  },
  setEthAccount({ commit }, data) {
    commit(SET_ETH_ACCOUNT, data);
  },
  setTotalBorrow({ commit }, data) {
    commit(SET_TOTAL_BORROW, data);
  }
};
