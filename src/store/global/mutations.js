import {
  SET_LOADER,
  SET_STATE,
  SET_ETH_ACCOUNT,
  SET_TOTAL_BORROW
} from "./mutation-types";

export default {
  [SET_LOADER](state, data) {
    state.loader = data;
  },
  [SET_STATE](state, data) {
    Object.keys(data).forEach(key => {
      state[key] = data[key];
    });
  },
  [SET_ETH_ACCOUNT](state, data) {
    state.ethAccount = data;
  },
  [SET_TOTAL_BORROW](state, data) {
    state.totalBorrow = data;
  }
};
