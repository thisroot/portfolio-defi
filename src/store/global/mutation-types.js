export const SET_LOADER = "SET_LOADER";
export const SET_STATE = "SET_STATE";
export const SET_ETH_ACCOUNT = "SET_ETH_ACCOUNT";
export const SET_TOTAL_BORROW = "SET_TOTAL_BORROW";
