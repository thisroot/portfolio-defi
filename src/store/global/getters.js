export default {
  loader: state => state.loader,
  web3: state => state.web3,
  ersdlPrice: state => state.ersdlPrice,
  ersdlBalance: state => state.ersdlBalance,
  unclaimedRewards: state => state.unclaimedRewards,
  ethAccount: state => state.ethAccount,
  totalBorrow: state => state.totalBorrow,
  totalSupply: state => state.totalSupply,
  netApy: state => state.netApy,
  liquidity: state => state.liquidity,
  borrowLimit: state => state.borrowLimit,
  network: state => state.network,
  networkId: state => state.networkId,
  chainId: state => state.chainId,
  tokensGeneratingRewards: state => state.tokensGeneratingRewards
};
