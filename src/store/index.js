import Vue from "vue";
import Vuex from "vuex";
import modal from "./modal";
import global from "./global";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    modal,
    global
  }
});

export default store;
