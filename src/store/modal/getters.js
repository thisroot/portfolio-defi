export default {
  current: state => state.current,
  modal: state => state.current,
  props: state => state.props || {},
  resolve: state => state.resolve,
  reject: state => state.reject
};
