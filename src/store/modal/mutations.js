import Vue from "vue";
import { SET_ALL, SET_PROPS } from "./mutation-types";

export default {
  [SET_ALL](state, data) {
    Object.assign(state, data);
    return state;
  },
  [SET_PROPS](state, props) {
    Vue.set(state, "props", props);
  }
};
