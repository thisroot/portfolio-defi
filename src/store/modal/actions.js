import { SET_ALL, SET_PROPS } from "./mutation-types";

export default {
  setCurrentModal({ commit }, current) {
    return new Promise((resolve, reject) => {
      commit(SET_ALL, { current, resolve, reject });
    });
  },
  setProps({ commit }, props) {
    // Do not set mouse event or etc as props
    // it happens when action to set some modal using in template without brackets
    if (props instanceof Event) return;
    commit(SET_PROPS, props);
  },
  setClaimModal({ dispatch }, props) {
    if (props) dispatch("setProps", props);
    return dispatch("setCurrentModal", "ClaimModal");
  },
  setDisconnectModal({ dispatch }, props) {
    if (props) dispatch("setProps", props);
    return dispatch("setCurrentModal", "DisconnectModal");
  },
  setCollateralModal({ dispatch }, props) {
    if (props) dispatch("setProps", props);
    return dispatch("setCurrentModal", "CollateralModal");
  },
  setTransactionModal({ dispatch }, props) {
    if (props) dispatch("setProps", props);
    return dispatch("setCurrentModal", "TransactionModal");
  },
  setBorrowModal({ dispatch }, props) {
    if (props) dispatch("setProps", props);
    return dispatch("setCurrentModal", "BorrowModal");
  },
  setSupplyModal({ dispatch }, props) {
    if (props) dispatch("setProps", props);
    return dispatch("setCurrentModal", "SupplyModal");
  },
  setConnectWalletModal({ dispatch }, props) {
    if (props) dispatch("setProps", props);
    return dispatch("setCurrentModal", "ConnectWalletModal");
  },
  closeModal({ commit, getters }) {
    const { props } = getters;
    commit(SET_ALL, {
      current: null,
      resolve: null,
      reject: null
    });
    // Timeout for clear props, because modal has close animation and text can blinking
    setTimeout(() => {
      // if props didn't changed, reset it
      if (props === getters.props) commit(SET_ALL, { props: {} });
    }, 200);
  }
};
