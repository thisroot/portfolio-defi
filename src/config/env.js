module.exports = {
  API_URL: process.env.VUE_APP_API_URL,
  NETWORK: process.env.VUE_APP_NETWORK,
  CHAIN_ID: process.env.VUE_APP_CHAIN_ID,
  NETWORK_ID: process.env.VUE_APP_NETWORK_ID,
  INFURA_ID: process.env.VUE_APP_INFURA_ID,
  REQUIRED_APPROVED_TRANSACTIONS: 1,
  COMPTROLLER_ADDRESS: process.env.VUE_APP_COMPTROLLER_ADDRESS,
  COMPOUNDLENS_ADDRESS: process.env.VUE_APP_COMPOUNDLENS_ADDRESS,
  ETHERSCAN_API_KEY: process.env.VUE_APP_ETHERSCAN_API_KEY,
  ETHERSCAN_URL: `https://api.etherscan.io/api?module=gastracker&action=gasoracle&apikey=${process.env.VUE_APP_ETHERSCAN_API_KEY}`
};
