import * as addrs from "@/config/env";

export default [
  {
    name: "ETH",
    apy: "9,98",
    cToken: addrs.CETHER_ADDRESS,
    token: addrs.CETHER_ADDRESS,
    liquidity: "17.9"
  },
  {
    name: "eRSDL",
    apy: "20,12",
    token: addrs.ERSDL_ADDRESS,
    cToken: addrs.CERSDL_ADDRESS,
    liquidity: "7.9"
  },
  {
    name: "DAI",
    apy: "21.16",
    token: addrs.DAI_ADDRESS,
    cToken: addrs.CDAI_ADDRESS,
    liquidity: "17.9"
  },
  {
    name: "USDC",
    apy: "54,21",
    token: addrs.USDC_ADDRESS,
    cToken: addrs.CUSDC_ADDRESS,
    liquidity: "17.9"
  },
  {
    name: "USDT",
    apy: "2,12"
  },
  {
    name: "USDT",
    apy: "2,12"
  },
  {
    name: "YFI",
    apy: "20,65",
    liquidity: "17.9"
  },
  {
    name: "WBTC",
    apy: "20,65",
    liquidity: "17.9"
  },
  {
    name: "AAVE",
    apy: "20,65",
    liquidity: "17.9"
  },
  {
    name: "SUSHI",
    apy: "20,65",
    liquidity: "17.9"
  },
  {
    name: "LINK",
    apy: "20,65",
    liquidity: "17.9"
  }
];
