import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/markets",
    name: "Markets",
    component: () =>
      import(/* webpackChunkName: "markets" */ "../views/Markets.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  routes
});

export default router;
