Running script: /compound-protocol/script/scen/deploy.scen...
Command: Gate (PriceOracle Address) (PriceOracle Deploy Simple)
Action: Deployed PriceOracle (Simple Price Oracle) to address 0x3be38479442b05B170FC0277fe7Dd040906E5589
Command: Gate (Comptroller Address) (Comptroller Deploy Standard Comptroller)
Action: Added Comptroller (Standard Comptroller Impl) at address 0x0CCDdb91B8DAF5134B28dF89B9a5f7fdBb036cFf
Command: Gate (Unitroller Address) (Unitroller Deploy)
Action: Added Unitroller (Unitroller) at address 0x8Afc410DD4c87d584974A609Ba39545e882A078c
Command: Gate (Timelock Address) (Timelock Deploy Admin 604800)
Action: Deployed Timelock to address 0xC4AE2Fb2A9136a079e1DF6261359379412B4D5E0
Command: Gate (InterestRateModel InterestRateModel Address) (InterestRateModel Deploy Fixed InterestRateModel 0.000004)
Action: Deployed interest rate model (Fixed rate 0.0004% per block) to address 0x56aFe7c660afD1c15D9ABDb87e1a37625dc9037B
Command: Gate (Erc20 ZRX Address) (Erc20 Deploy Standard ZRX "0x")
Action: Added ERC-20 token ZRX (Standard) at address 0x1E25845936060F0943B8eF7aDffEb1cFF4B14Bf9
Command: Gate (Erc20 BAT Address) (Erc20 Deploy NonStandard BAT "Basic Attention Token")
Action: Added ERC-20 token BAT (NonStandard) at address 0x5d528998E613bdF355E4192D96cEdEf005BD9831
Command: Gate (Erc20 DAI Address) (Erc20 Deploy Standard DAI "Dai")
Action: Added ERC-20 token DAI (Standard) at address 0x9E1329F4C80eB760670a3bC5Bf7DD6BAcf98Cba1
Command: Gate (Erc20 REP Address) (Erc20 Deploy Standard REP "Augur")
Action: Added ERC-20 token REP (Standard) at address 0xaaaa1424c9A2B465266e3d5B263aA5709278aA93
Command: Gate (Erc20 USDC Address) (Erc20 Deploy Standard USDC "USD Coin" 6)
Action: Added ERC-20 token USDC (Standard) at address 0x47b29C9feD6dAA88a93bb5Cbc6b000b1dc140852
Command: Gate (CToken cZRX Address) (CToken Deploy CErc20 cZRX "Test 0x 📈" (Erc20 ZRX Address) (Comptroller Address) (InterestRateModel InterestRateModel Address) 0.2e9 8 (Timelock Address))
Action: Added cToken Test 0x 📈 (CErc20<decimals=8>) at address 0x25CE4d06c3ED44e77Ff0f010d82840faCe49b2B0
Command: Gate (CToken cBAT Address) (CToken Deploy CErc20 cBAT "Test Basic Attention Token 📈" (Erc20 BAT Address) (Comptroller Address) (InterestRateModel InterestRateModel Address) 0.2e9 8 (Timelock Address))
Action: Added cToken Test Basic Attention Token 📈 (CErc20<decimals=8>) at address 0x76E79BeBc9D68edf3e89BefC969367CD0F186F40
Command: Gate (CToken cDAI Address) (CToken Deploy CErc20 cDAI "Test Dai 📈" (Erc20 DAI Address) (Comptroller Address) (InterestRateModel InterestRateModel Address) 0.2e9 8 (Timelock Address))
Action: Added cToken Test Dai 📈 (CErc20<decimals=8>) at address 0xF7361C74E3101702c2733147f8Cc74ca25C33d56
Command: Gate (CToken cREP Address) (CToken Deploy CErc20 cREP "Test Augur 📈" (Erc20 REP Address) (Comptroller Address) (InterestRateModel InterestRateModel Address) 0.2e9 8 (Timelock Address))
Action: Added cToken Test Augur 📈 (CErc20<decimals=8>) at address 0x71Fc98051fffa905E6559614ae61e0013147EEb1
Command: Gate (CToken cETH Address) (CToken Deploy CEther cETH "Test Ether 📈" (Comptroller Address) (InterestRateModel InterestRateModel Address) 0.2e9 8 (Timelock Address))
Action: Added cToken Test Ether 📈 (CEther<decimals=8>) at address 0x336052e720B52035224d4cD855B1D8daE22BC952
Command: Gate (CToken cUSDC Address) (CToken Deploy CErc20 cUSDC "Test USD Coin 📈" (Erc20 USDC Address) (Comptroller Address) (InterestRateModel InterestRateModel Address) 2e-4 8 (Timelock Address))
Action: Added cToken Test USD Coin 📈 (CErc20<decimals=8>) at address 0x3B40e7ddd9cB64c80c40E6277796629b8E025eD0
Command: Gate (Maximillion Address) (Maximillion Deploy cETH)
Action: Added Maximillion (Maximillion) at address 0x537B6A11CeA6E0B2a8bB62087d41dD014D99a436
Command: Print "Deployed Comptroller and cTokens: cETH, cBAT, cDAI, cREP, cUSDC and cZRX"
Deployed Comptroller and cTokens: cETH, cBAT, cDAI, cREP, cUSDC and cZRX


Command: Gate (Erc20 eRSDL Address) (Erc20 Deploy Standard eRSDL "UnFederalReserveToken")
Action: Added ERC-20 token eRSDL (Standard) at address 0x09f9B19AB6397FbA1c4D3aC46826F9223d59D075
Command: Gate (CToken eRSDL Address) (CToken Deploy CErc20 eRSDL "UnFederalReserveToken" (Erc20 eRSDL Address) (Comptroller Address) (InterestRateModel InterestRateModel Address) 0.2e9 8 (Timelock Address))
Action: Added cToken UnFederalReserveToken (CErc20<decimals=8>) at address 0x19aC0819718a7e0C584286C726BBa0834bcc55F2